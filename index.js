/*
	DATA persistence via Mongoose ODM / translating JS object to manage a database

	ODM - object document mapper is a tool that translates object in code to docuyments

	Mongoose - an ODM library that manages data relationship validates schemas and simplifies mongodb document manipulation via the use of models
	// using Mongoose and express.js mapapadali buhay natin

	Schemas - a schema is a representation of a documents structure. it also contains a documents expected properties and data types - create a document structure

	Models - a programing interface for querying or manipulating a database. A Mongoose model contaiuns method that simplify such operations
	
	npm init -y - initialize
	npm install express
	npm install mongoose
	touch .gitignore
*/

const express = require("express");

// mongoose is a package that allows creation of Schemas to model our data structures
// Also has access to a number of methods specially for manipulating database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// [SECTION] MongoDB Connection 
// mongodb+srv://admin:<password>@batch230.1dtuoqz.mongodb.net/?retryWrites=true&w=majority from MongoDB website >> connect using application as we are using an application

// Connect to the database by passing in your connection string, remember from robo3t replace the password and database names with actual values
// Syntax 
	// mongoose.connect("<MongoDB connection string>", {urlNewURLParser: true});

mongoose.connect("mongodb+srv://admin:admin@batch230.1dtuoqz.mongodb.net/S35?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connection to database
// Also allows to handle errors when the intial connection is established
// Works with the on and once Mongoose methods
let db = mongoose.connection

// If a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in browser console and in the terminal
db.on("error", console.error.bind(console, "connection error")); // to have a connection error message
db.once("open", () => console.log(`We're connected to the cloud database`)); // to have a connection message

app.use(express.json());

// Schema - we are creating rules for our fielt ex: name it should be on string, by defuaul if user didnt provide anything it will be pending
const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		default: "pending"
	}
}) 

const Task = mongoose.model("Task", taskSchema);

app.post("/tasks", (request, response) => { // app.post - para makapag push taus
	// Check if there are duplicate task
	Task.findOne({name: request.body.name}, (err, result) => {
		// If there was found and the documents name matches the infromation via the client/Postman
		if(result != null && result.name == request.body.name){
			return response.send("Duplicate task found");
		}
		else{
			let newTask = new Task({
				name: request.body.name
			})

			newTask.save((saveErr, savedTask) =>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return response.status(201).send("New task created");
				}
			})
		}
	})
}) 

app.get("/tasks", (request, response) =>{
	Task.find({}, (err, result) => {

		if(err){
			return console.log(err);
		}
		else{
			return response.status(200).json({
				data : result
			})
		}
	})
})

	// Imagine task, isa na po xang variables that contain whole collections it contains many object / task collection


/*
1. Add a functionality to check if there are duplicate tasks
	- If the user already exists in the database, we return an error
	- If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new User object with a "username" and "password" fields/properties
*/



const userSchema = new mongoose.Schema({

	username: String,
	password: String,
}) 

const User = mongoose.model("User", userSchema);

app.post("/signup", (request, response) => { 
	User.findOne({username: request.body.username}, (err, result) => {
		if(result != null && result.username == request.body.username){
			return response.send("Duplicate username found");
		}
		else{
			let newUser = new User({
				username: request.body.username,
				password: request.body.password
			})

			newUser.save((saveErr, savedTask) =>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return response.status(201).send("New user registered");
				}
			})
		}
	})
}) 



app.listen(port, () => console.log(`Server running at port ${port}`));
// to start application go to your terminal and type in nodemon + server(index.js)

/*
 if there is no process theres no need for curly brace 
 (parameter1, paramater2) => parameter1 + parameter2

 for arrow function that multiple statement you need to have a curly braces and return 
 (parameter1, paramater2) => {
	let sum = parameter1 + parameter2;
	return sum;
 }
 */



